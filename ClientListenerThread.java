import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientListenerThread implements Runnable {

    private ArrayList<MPacket> clientQueue = new ArrayList<MPacket>();
    private Hashtable<String, Integer> clientIDtable;
    private BlockingQueue<MPacket> toSendQueue;
    private CopyOnWriteArrayList<MPacket> requestQueue;
    private CopyOnWriteArrayList<MPacket> replyQueue;
    private CopyOnWriteArrayList<MPacket> notACKedQueue;
    private CopyOnWriteArrayList<MPacket> toExecuteQueue;
    //Max Clients
    private int MAX_CLIENTS;
    private MSocket mSocket  =  null;
    private Hashtable<String, Client> clientTable = null;
    private int actNext = 0;
    private Maze maze = null;
    //Broadcast global sequence number

    //Expected global sequence number

    //lamport timestamp
    private AtomicInteger timestamp;
    private Player[] players = null;
    private String name;
    private AtomicInteger missileTickOwner;
    private AtomicInteger localSendSequenceNumber;
    private AtomicInteger BGSN;
    private AtomicInteger EGSN;
    private Hashtable<String, MSocket> clientSocTable;



    public ClientListenerThread(MSocket mSocket,
    							int MAX_CLIENTS,
                                Hashtable<String, Client> clientTable,
                                Hashtable<String, Integer> clientIDtable,
                                BlockingQueue<MPacket> toSendQueue,
                                Maze maze,
                                Player[] players,
                                String name,
                                CopyOnWriteArrayList<MPacket> requestQueue,
                                CopyOnWriteArrayList<MPacket> replyQueue,
                                CopyOnWriteArrayList<MPacket> toExecuteQueue,
                                AtomicInteger timestamp,
                                AtomicInteger missileTickOwner,
                                AtomicInteger localSendSequenceNumber,
                                AtomicInteger BGSN,
                                AtomicInteger EGSN,
                                CopyOnWriteArrayList<MPacket> notACKedQueue,
                                Hashtable<String, MSocket> clientSocTable){
        this.mSocket = mSocket;
        this.MAX_CLIENTS = MAX_CLIENTS;
        this.clientTable = clientTable;
        this.maze=maze;
        this.clientIDtable = clientIDtable;
        this.toSendQueue = toSendQueue;
        this.toExecuteQueue = toExecuteQueue;
        this.players = players;
        this.name = name;
        this.requestQueue = requestQueue;
        this.replyQueue = replyQueue;
        this.timestamp = timestamp;
        this.notACKedQueue = notACKedQueue;
        this.missileTickOwner = missileTickOwner;
        this.localSendSequenceNumber = localSendSequenceNumber;
        this.BGSN = BGSN;
        this.EGSN = EGSN;
        this.clientSocTable = clientSocTable;
        if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }

    public void run() {
        MPacket received = null;
        Client client = null;
        if(Debug.debug) System.out.println("Starting ClientListenerThread");
        while(true){
        	if(Debug.debug) System.out.println("looping");
            try {
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //add lamport timestamp after!!!!!!!!!!!!!!!!!!!!!!!
                //ignoring delays and packet loss for now
            	if(Debug.debug) System.out.println("going into readObject() in ClientLister");
                
            	if(Debug.debug)
            		System.out.println("request queue: "+requestQueue);
            	if(Debug.debug)
            		System.out.println("reply queue: "+replyQueue);
            	
            	MPacket newPacket = (MPacket) mSocket.readObject();

                if(newPacket.type != MPacket.ACK && newPacket.type != MPacket.HELLO){
                    //System.out.println("reply to: "+this.name +" with sequence number "+newPacket.localSendSequenceNumber);
                    toSendQueue.add(new MPacket(this.name, MPacket.ACK, newPacket.name, MPacket.UNICAST, newPacket.localSendSequenceNumber));
                    //clientSocTable.get(newPacket.name).writeObject(new MPacket(this.name, MPacket.ACK, newPacket.name, MPacket.UNICAST, newPacket.localSendSequenceNumber));
                }
                //increment timestamp by 1
                this.timestamp.set(Math.max(this.timestamp.get(),newPacket.timestamp)+1);

                if(Debug.debug) System.out.println("mSocket: "+mSocket);
                if(Debug.debug) System.out.println("Sequence number is " + newPacket.sequenceNumber);
                if(Debug.debug) 
                	System.out.println("Received " + newPacket.toString());
                if(Debug.debug) System.out.println(newPacket.type);
                //1. check if it is an action request
                if (newPacket.type == MPacket.ACTIONREQUEST) {
                	//if(Debug.debug) 
                		System.out.println("Packet is ACTIONREQUEST "+newPacket.event);
                    // //1. check if the sender is itself
                    // if(newPacket.name == this.name){
                    //     //do nothing
                    //     //clientsenderthread should put the packet into the waiting queue already
                    // }

//                    if request queue is empty, reply OK
                    if (requestQueue.isEmpty()) {
                    	//if(Debug.debug) 
                    		System.out.println("request queue is empty. Reply OK to " + newPacket.requestLocalSequenceNumber);
                        //reply OK and increase BGSN
                        MPacket replyPacket = new MPacket(this.name, MPacket.ACTIONREPLY, newPacket.event, newPacket.name, MPacket.UNICAST);
                        replyPacket.requestLocalSequenceNumber= newPacket.requestLocalSequenceNumber;
                        replyPacket.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
                        toSendQueue.put(replyPacket);
                        this.BGSN.incrementAndGet();
//                    	if (Debug.debug)
                    		System.out.println("sent reply SEQNUM: "+replyPacket.requestLocalSequenceNumber);

                    }
                    //compare with your earliest request queue and if the incoming request 
                    // is earlier reply OK and increment this peers BGSN
                    else if (newPacket.timestamp < requestQueue.get(0).timestamp) {
//                    	if(Debug.debug) 
                    		System.out.println("request queue is not empty but this is faster. Reply OK");
                        MPacket replyPacket = new MPacket(this.name, MPacket.ACTIONREPLY, newPacket.event, newPacket.name, MPacket.UNICAST);
                        replyPacket.requestLocalSequenceNumber = newPacket.requestLocalSequenceNumber;
                        replyPacket.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
                        toSendQueue.put(replyPacket);
                        this.BGSN.incrementAndGet();
//                    	if (Debug.debug)
                    		System.out.println("sent reply SEQNUM: "+replyPacket.requestLocalSequenceNumber);
                    }
                    //compare with your request queue and if the incoming request
                    // is later, put the request into your reply queue
                    else if (newPacket.timestamp > requestQueue.get(0).timestamp) {
//                    	if(Debug.debug) 
                    		System.out.println("SEQNUM: "+newPacket.requestLocalSequenceNumber+" put into reply queue");
                        addReplyQueue(newPacket);
                    } else if (newPacket.timestamp == requestQueue.get(0).timestamp) {
                        //compare ID
                    	if(Debug.debug)
                    		System.out.println("same timestamp, compare timestamp");
//
                        if (clientIDtable.get(newPacket.name) < clientIDtable.get(requestQueue.get(0).name)) {
                        	MPacket replyPacket = new MPacket(this.name, MPacket.ACTIONREPLY, newPacket.event, newPacket.name, MPacket.UNICAST);
                            replyPacket.requestLocalSequenceNumber = newPacket.requestLocalSequenceNumber;
                            replyPacket.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
                            toSendQueue.put(replyPacket);
                            this.BGSN.incrementAndGet();
//                        	if (Debug.debug)
                        		System.out.println("sent reply SEQNUM: "+replyPacket.requestLocalSequenceNumber);
                        } else
                            addReplyQueue(newPacket);
                    }
                }
                else if (newPacket.type == MPacket.ACK) {
                    int index = -1;
                    int allreply = 0;
                    System.out.println("Received ACK from " + newPacket.name + " acking " + newPacket.localSendSequenceNumber);
                    MPacket change = null;
                    if(notACKedQueue.isEmpty())
                        continue;
                    for(MPacket item: notACKedQueue){
                        System.out.println("queue seq is " + item.localSendSequenceNumber + " ack is " + newPacket.localSendSequenceNumber);
                        if(item.localSendSequenceNumber == newPacket.localSendSequenceNumber){
                            change = item;
                            index = notACKedQueue.indexOf(item);
                            break;
                        }
                    }
                    //System.out.println("gonna change " + change.name);
                    if(change == null)
                        continue;
                    if(index >= 0)
                        change.acked[clientIDtable.get(change.name)] = 1;
                    for(int i = 0; i < MAX_CLIENTS; i++){
                        allreply += change.acked[i];
                        if(allreply >= MAX_CLIENTS - 1) {
                            notACKedQueue.remove(index);
                            break;
                        }
                    }
                }
                //2. check if it is an action reply
                else if (newPacket.type == MPacket.ACTIONREPLY) {
                	if(Debug.debug)
                		System.out.println("Action Reply");
                    //1. put it into your waiting queue and check if all replies are received
                    int checkallreply = addReply(newPacket);
                    //if(Debug.debug) 
                    	System.out.println("All responses collected: "+checkallreply+" for REQSEQ: "+newPacket.requestLocalSequenceNumber);
                    //2. if all replied OK, send out action packet
                    if (checkallreply == 1 && newPacket.requestLocalSequenceNumber == requestQueue.get(0).requestLocalSequenceNumber) {
                    	if(Debug.debug)
                    		System.out.println("Going to multicast action: "+ requestQueue.get(0).event);

                    	MPacket send = new MPacket(this.name, MPacket.ACTION, requestQueue.get(0).event, this.name, MPacket.MULTICAST);
                        send.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
                        //increment BGSN and assign it to the packet
                        send.BGSN=this.BGSN.incrementAndGet();
                    	toSendQueue.add(send);



//                    	if (Debug.debug)
                    		System.out.println("sent Action Multicast SEQNUM: "+send.localSendSequenceNumber);

//                    	if (Debug.debug)
                    		System.out.println("local action executing");

                		if(send.BGSN < this.EGSN.get()){
                    		System.out.println("duplicate or wrong packet received. Discarded. "+send.toString());
                    		System.out.println("!!!!!!!!!!!!!!Expected "+this.EGSN.get()+" but got "+send.BGSN+"!!!!!!!!!!!!!!!!");
                    		continue;
                    	}else if(send.BGSN > this.EGSN.get()){
                    		System.out.println("Expected "+this.EGSN.get()+" but got "+send.BGSN+" queueing this packet????????????????");
                    		addToExecuteQueue(send);
                    		requestQueue.remove(0);
                    		//continue;
                    	}
                    	else{
                    		System.out.println("All Good. Expected: "+this.EGSN.get()+" and got "+send.BGSN);
                    		//execute this myself
                            //assign client
                        	client = clientTable.get(requestQueue.get(0).name);
                            //1. Execute the action
                            if (requestQueue.get(0).event == MPacket.UP) {
                                client.forward();
                            } else if (requestQueue.get(0).event == MPacket.DOWN) {
                                client.backup();
                            } else if (requestQueue.get(0).event == MPacket.LEFT) {
                                client.turnLeft();
                            } else if (requestQueue.get(0).event == MPacket.RIGHT) {
                                client.turnRight();
                            } else if (requestQueue.get(0).event == MPacket.FIRE) {
                                client.fire();
                                //maze.missileTick();
                            } else if (requestQueue.get(0).event == MPacket.MISSILETICK) {
                                //System.out.println("received Missle Tick packet"+this);
                                maze.missileTick();
                                //client.missileTick();
                                //run pretty much the same code as lab 2
                            } else {
                                throw new UnsupportedOperationException();
                            }
//                            if (Debug.debug)
                        		System.out.println("local action executed");
                            this.EGSN.incrementAndGet();
                            requestQueue.remove(0);
                    	}

                        
                        //check if the next one got all replies yet
                        while(!requestQueue.isEmpty() &&  checkReplies(requestQueue.get(0)) == 1){
                        	MPacket send1 = new MPacket(this.name, MPacket.ACTION, requestQueue.get(0).event, this.name, MPacket.MULTICAST);
                        	//increment and assign it to the packet
                        	send1.BGSN = this.BGSN.incrementAndGet();
                            send1.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
                        	toSendQueue.add(send1);
//                        	if (Debug.debug)
                        		System.out.println("sent Action Multicast SEQNUM: "+send1.localSendSequenceNumber);
//                        	if (Debug.debug)
                        		System.out.println("local action executing");

                    		if(send1.BGSN < this.EGSN.get()){
                        		System.out.println("duplicate or wrong packet received. Discarded. "+send1.toString());
                        		System.out.println("!!!!!!!!!!!!!!Expected "+this.EGSN.get()+" but got "+send1.BGSN+"!!!!!!!!!!!!!!!!");
                        		continue;
                        	}else if(send1.BGSN > this.EGSN.get()){
                        		System.out.println("Expected "+this.EGSN.get()+" but got "+send1.BGSN+" queueing this packet????????????????");
                        		addToExecuteQueue(send1);
                        		requestQueue.remove(0);
                        		//continue;
                        	}
                        	else{
                        		System.out.println("All Good. Expected: "+this.EGSN.get()+" and got "+send1.BGSN);
                        		//execute this myself
                                //assign client
                            	client = clientTable.get(requestQueue.get(0).name);
                                //1. Execute the action
                                if (requestQueue.get(0).event == MPacket.UP) {
                                    client.forward();
                                } else if (requestQueue.get(0).event == MPacket.DOWN) {
                                    client.backup();
                                } else if (requestQueue.get(0).event == MPacket.LEFT) {
                                    client.turnLeft();
                                } else if (requestQueue.get(0).event == MPacket.RIGHT) {
                                    client.turnRight();
                                } else if (requestQueue.get(0).event == MPacket.FIRE) {
                                    client.fire();
                                    //maze.missileTick();
                                } else if (requestQueue.get(0).event == MPacket.MISSILETICK) {
                                    //System.out.println("received Missle Tick packet"+this);
                                    maze.missileTick();
                                    //client.missileTick();
                                    //run pretty much the same code as lab 2
                                } else {
                                    throw new UnsupportedOperationException();
                                }
//                                if (Debug.debug)
                            		System.out.println("local action executed");
                                this.EGSN.incrementAndGet();
                                requestQueue.remove(0);
                        	}


                        }
                        System.out.println("dequeue replying queue");
                        while(!replyQueue.isEmpty()){
//                        	if (Debug.debug)

                        	// if queue is empty
                        	if(!requestQueue.isEmpty()){
                        		if(requestQueue.get(0).timestamp < replyQueue.get(0).timestamp){
//                                	if (Debug.debug)
                            		System.out.println("here1");
                        			break;
                        		}else if(requestQueue.get(0).timestamp == replyQueue.get(0).timestamp && clientIDtable.get(newPacket.name) > clientIDtable.get(requestQueue.get(0).name)){
//                                	if (Debug.debug)
                            		System.out.println("here2");
                        			break;
                        		}else{
//                        			if (Debug.debug)
                                		System.out.println("replied");
                            		MPacket replyPacket = new MPacket(this.name, MPacket.ACTIONREPLY, replyQueue.get(0).event, replyQueue.get(0).name, MPacket.UNICAST);
    	                        	replyPacket.requestLocalSequenceNumber = replyQueue.get(0).requestLocalSequenceNumber;
    	                            replyPacket.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
    	                            toSendQueue.put(replyPacket);
    	                            this.BGSN.incrementAndGet();
//    	                        	if (Debug.debug)
    	                        		System.out.println("1 sent reply SEQNUM: "+replyPacket.requestLocalSequenceNumber);
    	                            replyQueue.remove(0);
                        		}
                        	}
                        	else{
//                        		if (Debug.debug)
                            		System.out.println("replied");
                        		MPacket replyPacket = new MPacket(this.name, MPacket.ACTIONREPLY, replyQueue.get(0).event, replyQueue.get(0).name, MPacket.UNICAST);
	                        	replyPacket.requestLocalSequenceNumber = replyQueue.get(0).requestLocalSequenceNumber;
	                            replyPacket.localSendSequenceNumber = this.localSendSequenceNumber.getAndIncrement();
	                            toSendQueue.put(replyPacket);
	                            this.BGSN.incrementAndGet();
//	                        	if (Debug.debug)
	                        		System.out.println("1 sent reply SEQNUM: "+replyPacket.requestLocalSequenceNumber);
	                            replyQueue.remove(0);
                        	}
                        }
                        
                    }

                }
                //3. check if it is an execution
                else if (newPacket.type == MPacket.ACTION) {
//                	if (Debug.debug)
                	System.out.println("Action Received");
                	if(newPacket.event != MPacket.MISSILETICK)
                		System.out.println("action executing");
                	//if not the expected one, discard this action
                	if(newPacket.BGSN < this.EGSN.get()){
                		System.out.println("duplicate or wrong packet received. Discarded. "+newPacket.toString());
                		System.out.println("!!!!!!!!!!!!!!Expected "+this.EGSN.get()+" but got "+newPacket.BGSN+"!!!!!!!!!!!!!!!!");
                		continue;
                	}else if(newPacket.BGSN > this.EGSN.get()){
                		System.out.println("Expected "+this.EGSN.get()+" but got "+newPacket.BGSN+" queueing this packet????????????????");
                		addToExecuteQueue(newPacket);
                		//continue;
                	}
                	else{
                		System.out.println("All Good. Expected: "+this.EGSN.get()+" and got "+newPacket.BGSN);
                		//assign client
                    	client = clientTable.get(newPacket.name);
                        //1. Execute the action
                        if (newPacket.event == MPacket.UP) {
                            client.forward();
                        } else if (newPacket.event == MPacket.DOWN) {
                            client.backup();
                        } else if (newPacket.event == MPacket.LEFT) {
                            client.turnLeft();
                        } else if (newPacket.event == MPacket.RIGHT) {
                            client.turnRight();
                        } else if (newPacket.event == MPacket.FIRE) {
                        	if(Debug.debug) System.out.println("client: "+client+" Fired");
                        	client.fire();
                            //maze.missileTick();
                        } else if (newPacket.event == MPacket.MISSILETICK) {
                            //System.out.println("received Missle Tick packet"+this);
                            maze.missileTick();
                            //client.missileTick();
                            //run pretty much the same code as lab 2
                        } else {
                            throw new UnsupportedOperationException();
                        }
//                        if (Debug.debug)
                        if(newPacket.event != MPacket.MISSILETICK)
                    		System.out.println("action executed");
                        this.EGSN.incrementAndGet();

                	}

                    //see if any other packets are supposed to be executed
                    while(!this.toExecuteQueue.isEmpty() && this.toExecuteQueue.get(0).BGSN <= this.EGSN.get()){

                    	if(this.toExecuteQueue.get(0).event != MPacket.MISSILETICK)
                    		System.out.println("action executing");
                    	//if not the expected one, discard this action
                    	if(this.toExecuteQueue.get(0).BGSN < this.EGSN.get()){
                    		System.out.println("duplicate or wrong packet received. Discarded. "+this.toExecuteQueue.get(0).toString());
                    		System.out.println("!!!!!!!!!!!!!!Expected "+this.EGSN.get()+" but got "+this.toExecuteQueue.get(0).BGSN+"!!!!!!!!!!!!!!!!");
                    		continue;
                    	}else if(this.toExecuteQueue.get(0).BGSN > this.EGSN.get()){
                    		System.out.println("Expected "+this.EGSN.get()+" but got "+this.toExecuteQueue.get(0).BGSN+" queueing this packet????????????????");
                    		addToExecuteQueue(this.toExecuteQueue.get(0));
                    		continue;
                    	}
                    	else{
                    		System.out.println("All Good. Expected: "+this.EGSN.get()+" and got "+this.toExecuteQueue.get(0).BGSN);
	                    	//assign client
	                    	System.out.println("Executing remaining packets---------------------------");
	                    	client = clientTable.get(this.toExecuteQueue.get(0).name);
	                        //1. Execute the action
	                        if (this.toExecuteQueue.get(0).event == MPacket.UP) {
	                            client.forward();
	                        } else if (this.toExecuteQueue.get(0).event == MPacket.DOWN) {
	                            client.backup();
	                        } else if (this.toExecuteQueue.get(0).event == MPacket.LEFT) {
	                            client.turnLeft();
	                        } else if (this.toExecuteQueue.get(0).event == MPacket.RIGHT) {
	                            client.turnRight();
	                        } else if (this.toExecuteQueue.get(0).event == MPacket.FIRE) {
	                        	if(Debug.debug) System.out.println("client: "+client+" Fired");
	                        	client.fire();
	                            //maze.missileTick();
	                        } else if (this.toExecuteQueue.get(0).event == MPacket.MISSILETICK) {
	                            //System.out.println("received Missile Tick packet"+this);
	                            maze.missileTick();
	                            //client.missileTick();
	                            //run pretty much the same code as lab 2
	                        } else {
	                            throw new UnsupportedOperationException();
	                        }
	//                        if (Debug.debug)
	                        if(this.toExecuteQueue.get(0).event != MPacket.MISSILETICK)
	                    		System.out.println("action executed");
	                        this.EGSN.incrementAndGet();
	                        toExecuteQueue.remove(0);
//	                      if (Debug.debug)
	                        System.out.println("toExecuteQueue: "+this.toExecuteQueue);


                    }
//                  if (Debug.debug)
                    System.out.println("Finished executing remaining ones. toExecuteQueue: "+this.toExecuteQueue);
                    }

                }
                //4. check if it is a HELLO packet
                else if (newPacket.type == MPacket.HELLO) {
                    if (newPacket.event == MPacket.HELLO_INIT) {
                        MPacket connect = new MPacket(name, MPacket.HELLO, MPacket.HELLO_RESP);
                        connect.communication=MPacket.UNICAST;
                        connect.sendto=newPacket.name;
                        toSendQueue.add(connect);
//                        mSocket.writeObject(connect);
                        if(Debug.debug) System.out.println("Receiving Hello from other client, sending response!!");
                    } else if (newPacket.event == MPacket.HELLO_RESP) {
                    	if(Debug.debug) System.out.println("Received Hello Response!!");
                        continue;
                    }
                }
                else if(newPacket.type == MPacket.MISSILETICKEOWNER){
                	this.missileTickOwner.set(1);
                }
            }catch(IOException e){
                e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }catch(InterruptedException e){
                e.printStackTrace();
            }
//                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                    System.out.println("Actnext is " + actNext);
//                    received = getNext();
//                    if (received == null) {
//                        received = (MPacket) mSocket.readObject();
//                        if (received.sequenceNumber != actNext) {
//                            addQueue(received);
//                            continue;
//                        }
//                    } else
//                        clientQueue.remove(0);
//
//                    System.out.println("Sequence number is " + received.sequenceNumber);
//                    System.out.println("Received " + received);
//                    client = clientTable.get(received.name);
//                    this.actNext++;
//                    if (received.event == MPacket.HELLO_INIT) {
//                        MPacket connect = new MPacket(MPacket.HELLO, MPacket.HELLO_RESP);
//                        mSocket.writeObject(connect);
//                        System.out.println("Receiving Hello from other client, sending response!!");
//                    } else if (received.event == MPacket.HELLO_RESP) {
//                        System.out.println("Received Hello Response!!");
//                        continue;
//                    } else if (received.event == MPacket.UP) {
//                        client.forward();
//                    } else if (received.event == MPacket.DOWN) {
//                        client.backup();
//                    } else if (received.event == MPacket.LEFT) {
//                        client.turnLeft();
//                    } else if (received.event == MPacket.RIGHT) {
//                        client.turnRight();
//                    } else if (received.event == MPacket.FIRE) {
//                        client.fire();
//                    } else if (received.event == MPacket.MISSLETICK) {
//                        System.out.println("received Missle Tick packet" + this);
//                        maze.missileTick();
//                        //client.missileTick();
//                    } else {
//                        throw new UnsupportedOperationException();
//                    }
//                }
//            }catch(IOException e){
//                e.printStackTrace();
//            }catch(ClassNotFoundException e){
//                e.printStackTrace();
//            }
        }
        //System.out.println("Should not reach here");
    }
    private void addReplyQueue(MPacket item){
    	boolean added = false;
        if(this.replyQueue != null){
        	if(!this.replyQueue.isEmpty()){
	            for(int i = 0 ; i<replyQueue.size(); i++){
	                if(item.timestamp < replyQueue.get(i).timestamp){
	                    replyQueue.add(i, item);
	                    added=true;
	                    break;
	                }else if(item.timestamp == replyQueue.get(i).timestamp){
	                    if(item.id < replyQueue.get(i).id)
	                    	replyQueue.add(i, item);
	                    else
	                    	replyQueue.add(i+1, item);
	                    added=true;
	                    break;
	                }else
	                    continue;
	            }
	            //add to the last
	            if(!added)
	            	replyQueue.add(item);
        	} else
        		replyQueue.add(item);

        }
    }
    private void addToExecuteQueue(MPacket item){
    	boolean added = false;
        if(this.toExecuteQueue != null){
        	if(!this.toExecuteQueue.isEmpty()){
	            for(int i = 0 ; i<toExecuteQueue.size(); i++){
	                if(item.BGSN < toExecuteQueue.get(i).BGSN){
	                	toExecuteQueue.add(i, item);
	                    added=true;
	                    break;
	                }else if(item.BGSN == toExecuteQueue.get(i).BGSN){
	                    if(item.id < toExecuteQueue.get(i).id)
	                    	toExecuteQueue.add(i, item);
	                    else
	                    	toExecuteQueue.add(i+1, item);
	                    added=true;
	                    break;
	                }else
	                    continue;
	            }
	            //add to the last
	            if(!added)
	            	toExecuteQueue.add(item);
        	} else
        		toExecuteQueue.add(item);

        }
       System.out.println("added a packet to executeQueue!!");
       System.out.println("toExecuteQueue: "+this.toExecuteQueue);

    }

    private MPacket getNext(){
        if(this.clientQueue != null && (this.clientQueue.size() > 0)) {
        	if(Debug.debug) System.out.println("Inside the Queue Checking "+clientQueue.size());
            if(this.clientQueue.get(0).sequenceNumber == this.actNext) {
                return clientQueue.get(0);
            }
        }
        return null;
    }
    private synchronized int addReply(MPacket packet){
    	if(requestQueue != null && !requestQueue.isEmpty()){
	        for (int i = 0; i < requestQueue.size(); i++){
	        	if(Debug.debug) System.out.println("current packet: "+requestQueue.get(i));
	            if(packet.requestLocalSequenceNumber == requestQueue.get(i).requestLocalSequenceNumber){
	            	if(Debug.debug) System.out.println("Found packet");
	            	requestQueue.get(i).replies[clientIDtable.get(packet.name)] = 1;
	            	if(Debug.debug) System.out.println("Added reply at index: "+requestQueue.get(i).replies[clientIDtable.get(packet.name)]);
	            	int allreply = 0;
	                for(int j = 0; j < MAX_CLIENTS;j++){
	                        allreply += requestQueue.get(i).replies[j];
	                }
	                if(allreply == MAX_CLIENTS-1)
	                    return 1;
	                else
	                    return 0;
	            }
	        }
    	}
        //No idea what should return here, but you have to handle the case if the for loop ends by accident.
        return -1;
    }
    private int checkReplies(MPacket packet){
    	int allreply=0;
    	for (int i = 0; i < MAX_CLIENTS; i++)
    		allreply += packet.replies[i];
    	
    	if(allreply == MAX_CLIENTS-1)
    		return 1;
    	else 
    		return 0;
    	
    }
}
