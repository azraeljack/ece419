import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientSenderThread implements Runnable {

    private ArrayList<MSocket> mSocket = new ArrayList<MSocket>();
    private BlockingQueue<MPacket> toSendQueue = null;
    private ArrayList<MPacket> waitingQueue = null;
    public static int clientSequence = 0;
    private int MAX_CLIENTS;
    private Hashtable<String, Client> clientTable;
    private Hashtable<String, Integer> clientIDtable;
    private Hashtable<String, MSocket> clientSocTable;
    private AtomicInteger timestamp;
    private CopyOnWriteArrayList<MPacket> notACKedQueue;

    public ClientSenderThread(ArrayList<MSocket> mSocket,
    						  int MAX_CLIENTS,
                              BlockingQueue toSendQueue,
                              Hashtable<String, Client> clientTable,
                              Hashtable<String, Integer> clientIDtable,
                              Hashtable<String, MSocket> clientSocTable,
                              AtomicInteger timestamp,
                              CopyOnWriteArrayList<MPacket> notACKedQueue){

        this.mSocket.addAll(mSocket);
        this.MAX_CLIENTS = MAX_CLIENTS;
        this.toSendQueue = toSendQueue;
        this.clientIDtable = clientIDtable;
        this.clientTable = clientTable;
        this.clientSocTable = clientSocTable;
        this.timestamp = timestamp;
        this.notACKedQueue = notACKedQueue;
    }

    public void run() {
        MPacket toServer = null;
        if(Debug.debug) System.out.println("Starting ClientSenderThread");
        while(true){
            try{
                MPacket packet;
                //Take packet from queue
                packet = (MPacket)toSendQueue.take();
                System.out.println("To sendqueue is " + toSendQueue.toString());
                if(Debug.debug) System.out.println("ClientSocTable: "+this.clientSocTable);
                //packet.sequenceNumber = clientSequence;
//                if (packet.type == MPacket.ACTIONREQUEST){
//                    //put this action request into waiting queue
//                    packet.replies=new int[MAX_CLIENTS];
//                    //initialize all responses to 0
//                    for(int i = 0; i<MAX_CLIENTS;i++){
//                        packet.replies[i]=0;
//                    }
//                    addWaitingQueue(packet);
//                }
                if(packet.type != MPacket.HELLO && packet.type != MPacket.ACK && !packet.resend){
                    packet.timestamp = this.timestamp.incrementAndGet();
                    packet.physicalTimestamp = System.currentTimeMillis();
                    packet.acked = new int[MAX_CLIENTS];
                    for (int i = 0; i < MAX_CLIENTS; i++)
                        packet.acked[i] = 0;
                    notACKedQueue.add(packet);
                }
                if(Debug.debug) System.out.println("Sending " + packet);

                if(packet.communication == MPacket.UNICAST){
                	if(Debug.debug) System.out.println("Sending a packet to "+clientSocTable.get(packet.sendto));
                    clientSocTable.get(packet.sendto).writeObject(packet);
                }
                else if(packet.communication == MPacket.MULTICAST){
                    for(MSocket m: mSocket){
                    	if(Debug.debug) System.out.println("Sending a packet to "+m);
                        m.writeObject(packet);
                    }
                }
                clientSequence++;
            }catch(InterruptedException e){
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }

        }
    }
    
}
