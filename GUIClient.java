/*
Copyright (C) 2004 Geoffrey Alan Washburn
      
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
      
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
      
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
*/

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An implementation of {@link LocalClient} that is controlled by the keyboard
 * of the computer on which the game is being run.  
 * @author Geoffrey Washburn &lt;<a href="mailto:geoffw@cis.upenn.edu">geoffw@cis.upenn.edu</a>&gt;
 * @version $Id: GUIClient.java 343 2004-01-24 03:43:45Z geoffw $
 */

public class GUIClient extends LocalClient implements KeyListener {

        /**
         * Create a GUI controlled {@link LocalClient}.  
         */

        private int id;
        
        private CopyOnWriteArrayList<MPacket> requestQueue = null;
        private BlockingQueue<MPacket> toSendQueue = null;
        private AtomicInteger timestamp;
        private AtomicInteger localSendSequenceNumber;
        private int MAX_CLIENTS;

        public GUIClient(String name, int MAX_CLIENTS, CopyOnWriteArrayList<MPacket> requestQueue, BlockingQueue<MPacket> toSendQueue , int id, AtomicInteger timestamp, AtomicInteger localSendSequenceNumber) {
                super(name);
                
                this.requestQueue = requestQueue;
                this.MAX_CLIENTS = MAX_CLIENTS;
                this.toSendQueue = toSendQueue;
                this.id = id;
                this.timestamp=timestamp;
                this.localSendSequenceNumber=localSendSequenceNumber;
        }
        
        /**
         * Handle a key press.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyPressed(KeyEvent e){
                try{
                		if(requestQueue.size() >= 50){
                			//dont do anything
                		}else{
                		
	                        // If the user pressed Q, invoke the cleanup code and quit. 
	                        if((e.getKeyChar() == 'q') || (e.getKeyChar() == 'Q')) {
	                                Mazewar.quit();
	                        // Up-arrow moves forward.
	                        } else if(e.getKeyCode() == KeyEvent.VK_UP) {
	                                //forward();
	                        	    if(Debug.debug) System.out.println("GUI sending action : UP");
	                                MPacket packet = new MPacket(getName(), MPacket.ACTIONREQUEST, MPacket.UP);
	                                packet.localSendSequenceNumber = localSendSequenceNumber.get();
	                                packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
	                                packet.replies=new int[MAX_CLIENTS];
									//initialize all responses to 0
									for(int i = 0; i<MAX_CLIENTS;i++){
									    packet.replies[i]=0;
									}
	                                requestQueue.add(packet);
	                                packet.communication= MPacket.MULTICAST;
	                                toSendQueue.put(packet);
	                                
	//                                if(Debug.debug)
	                                	System.out.println("GUI requesting action : UP SEQNUM: "+packet.requestLocalSequenceNumber);
	                                	
	                        // Down-arrow moves backward.
	                        } else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
	                                //backup();
	                        		if(Debug.debug) System.out.println("GUI sending action : DOWN");
	                                MPacket packet = new MPacket(getName(), MPacket.ACTIONREQUEST, MPacket.DOWN);
	                                packet.localSendSequenceNumber = localSendSequenceNumber.get();
	                                packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
	                                packet.replies=new int[MAX_CLIENTS];
									//initialize all responses to 0
									for(int i = 0; i<MAX_CLIENTS;i++){
									    packet.replies[i]=0;
									}
	                                requestQueue.add(packet);
	                                packet.communication= MPacket.MULTICAST;
	                                toSendQueue.put(packet);
	                                
	//                              if(Debug.debug)
	                              	System.out.println("GUI requesting action : UP SEQNUM: "+packet.requestLocalSequenceNumber);
	                              	
	                        // Left-arrow turns left.
	                        } else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
	                                //turnLeft();
	                        		if(Debug.debug) System.out.println("GUI sending action : LEFT");
	                                MPacket packet = new MPacket(getName(), MPacket.ACTIONREQUEST, MPacket.LEFT);
	                                packet.localSendSequenceNumber = localSendSequenceNumber.get();
	                                packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
	                                packet.replies=new int[MAX_CLIENTS];
									//initialize all responses to 0
									for(int i = 0; i<MAX_CLIENTS;i++){
									    packet.replies[i]=0;
									}
	                                requestQueue.add(packet);
	                                packet.communication= MPacket.MULTICAST;
	                                toSendQueue.put(packet);
	                                
	//                              if(Debug.debug)
	                              	System.out.println("GUI requesting action : UP SEQNUM: "+packet.requestLocalSequenceNumber);
	                              	
	                        // Right-arrow turns right.
	                        } else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
	                                //turnRight();
	                        		if(Debug.debug) System.out.println("GUI sending action : RIGHT");
	                                MPacket packet = new MPacket(getName(), MPacket.ACTIONREQUEST, MPacket.RIGHT);
	                                packet.localSendSequenceNumber = localSendSequenceNumber.get();
	                                packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
	                                packet.replies=new int[MAX_CLIENTS];
									//initialize all responses to 0
									for(int i = 0; i<MAX_CLIENTS;i++){
									    packet.replies[i]=0;
									}
	                                requestQueue.add(packet);
	                                packet.communication= MPacket.MULTICAST;
	                                toSendQueue.put(packet);
	                                
	//                              if(Debug.debug)
	                              	System.out.println("GUI requesting action : UP SEQNUM: "+packet.requestLocalSequenceNumber);
	                              	
	                        // Spacebar fires.
	                        } else if(e.getKeyCode() == KeyEvent.VK_SPACE) {
	                                //fire();
	                    			if(Debug.debug) System.out.println("GUI sending action : FIRE");
	                                MPacket packet = new MPacket(getName(), MPacket.ACTIONREQUEST, MPacket.FIRE);
	                                packet.localSendSequenceNumber = localSendSequenceNumber.get();
	                                packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
	                                packet.replies=new int[MAX_CLIENTS];
									//initialize all responses to 0
									for(int i = 0; i<MAX_CLIENTS;i++){
									    packet.replies[i]=0;
									}
	                                requestQueue.add(packet);
	                                packet.communication= MPacket.MULTICAST;
	                                toSendQueue.put(packet);
	                                
	//                              if(Debug.debug)
	                              	System.out.println("GUI requesting action : UP SEQNUM: "+packet.requestLocalSequenceNumber);
	                              	
	                        }
	                        localSendSequenceNumber.incrementAndGet();
                		}
                }catch(InterruptedException ie){
                        //An exception is caught, do something
                        Thread.currentThread().interrupt();
                }
        }
        
        /**
         * Handle a key release. Not needed by {@link GUIClient}.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyReleased(KeyEvent e) {
        }
        
        /**
         * Handle a key being typed. Not needed by {@link GUIClient}.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyTyped(KeyEvent e) {
        }

}
