import java.io.Serializable;
import java.util.Collection;

public class MPacket implements Serializable {

    /*The following are the type of events*/
    public static final int HELLO = 100;
    public static final int ACTION = 200;
    public static final int ACTIONREQUEST = 300;
    public static final int ACTIONREPLY = 400;
    public static final int ACK = 800;
    
    /*The following are the specific action 
    for each type*/
    /*Initial Hello*/
    public static final int HELLO_INIT = 101;
    /*Response to Hello*/
    public static final int HELLO_RESP = 102;

    /*Action*/
    public static final int UP = 201;
    public static final int DOWN = 202;
    public static final int LEFT = 203;
    public static final int RIGHT = 204;
    public static final int FIRE = 205;
    public static final int MISSILETICK = 206;

    public static final int MULTICAST=501;
    public static final int UNICAST=502;
    
    public static final int MISSILETICKEOWNER = 600;
    
    
    //These fields characterize the event  
    public int type;
    public int event;

    //The name determines the client that initiated the event
    public String name;
    public int id;
    public String senderHostName;
    public int senderPort;

    //For sending out packet
    public String sendto;
    public MPacket packet;
    public int communication;

    //The sequence number of the event
    public int sequenceNumber;
    public int replySequenceNumber;
    //Lamport timestamp
    public int timestamp;
    //localhost sequence number to distinguish each packet sent from a node
    public int requestLocalSequenceNumber;
    public int localSendSequenceNumber;
    
    public int BGSN;
    public int EGSN;
    
    public long physicalTimestamp = 0;
    public boolean resend = false;
    
    

    //These are used to initialize the board
    public int mazeSeed;
    public int mazeHeight;
    public int mazeWidth; 
    public Player[] players;
    public int[] replies;
    public int[] acked;

    public MPacket(int type, int event){
        this.type = type;
        this.event = event;
    }
    
    public MPacket(String name, int type, int event){
        this.name = name;
        this.type = type;
        this.event = event;
    }

    public MPacket(String name, int type, String sendto, int communication, int localSendSequenceNumber){
        this.name = name;
        this.type = type;
        this.sendto = sendto;
        this.communication = communication;
        this.localSendSequenceNumber = localSendSequenceNumber;
    }

    public MPacket(String name, int type, int event, String sendto, int communication){
        this.name = name;
        this.type = type;
        this.event = event;
        this.sendto = sendto;
        this.communication = communication;
    }

    public void setTimestamp(int timestamp){
        this.timestamp = timestamp;
    }
    
    public void setReplySequenceNumber(int replySequenceNumber){
        this.replySequenceNumber=replySequenceNumber;
    }

    public String toString(){
        String typeStr;
        String eventStr;
        
        switch(type){
            case 100:
                typeStr = "HELLO";
                break;
            case 200:
                typeStr = "ACTION";
                break;
            case 300:
                typeStr = "ACTIONREQUEST";
                break;
            case 400:
                typeStr = "ACTIONREPLY";
                break;
            case 800:
                typeStr = "ACK";
            default:
                typeStr = "ERROR";
                break;        
        }
        switch(event){
            case 101:
                eventStr = "HELLO_INIT";
                break;
            case 102:
                eventStr = "HELLO_RESP";
                break;
            case 201:
                eventStr = "UP";
                break;
            case 202:
                eventStr = "DOWN";
                break;
            case 203:
                eventStr = "LEFT";
                break;
            case 204:
                eventStr = "RIGHT";
                break;
            case 205:
                eventStr = "FIRE";
                break;
            case 206:
                eventStr = "MISSLETICK";
                break;
            default:
                eventStr = "ERROR";
                break;        
        }
        //MPACKET(NAME: name, <typestr: eventStr>, SEQNUM: sequenceNumber)
        String retString = String.format("MPACKET(NAME: %s, <%s: %s>, SEQNUM: %s, REQSEQ: %s, TS: %s BGSN: %s)", name, 
            typeStr, eventStr, localSendSequenceNumber, requestLocalSequenceNumber, timestamp, BGSN);
        return retString;
    }


}


