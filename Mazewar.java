/*
Copyright (C) 2004 Geoffrey Alan Washburn
   
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
   
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
   
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
*/
  
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.PriorityQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The entry point and glue code for the game.  It also contains some helpful
 * global utility methods.
 * @author Geoffrey Washburn &lt;<a href="mailto:geoffw@cis.upenn.edu">geoffw@cis.upenn.edu</a>&gt;
 * @version $Id: Mazewar.java 371 2004-02-10 21:55:32Z geoffw $
 */

public class Mazewar extends JFrame {

        /**
         * The default width of the {@link Maze}.
         */
        private final int mazeWidth = 20;

        /**
         * The default height of the {@link Maze}.
         */
        private final int mazeHeight = 10;

        /**
         * The default random seed for the {@link Maze}.
         * All implementations of the same protocol must use 
         * the same seed value, or your mazes will be different.
         */
        private final int mazeSeed = 42;

        /**
         * The {@link Maze} that the game uses.
         */
        private Maze maze = null;

        /**
         * The Mazewar instance itself. 
         */
        private static final int MAX_CLIENTS = 2;
        private Mazewar mazewar = null;
        private ArrayList<MSocket> mSocket = new ArrayList<MSocket>();
        private Socket nSocket = null;
        private MServerSocket lSocket = null;
        private ObjectOutputStream out = null;
        private ObjectInputStream in = null;
        private Player[] players = null;
        private String name;
        private String playerHostName;
        private int playerPort;


        /**
         * The {@link GUIClient} for the game.
         */
        private GUIClient guiClient = null;
        
        private int id;
        /**
         * A map of {@link Client} clients to client name.
         */
        private Hashtable<String, Client> clientTable = null;
        private Hashtable<String, Integer> clientIDTable = null;
        private Hashtable<Integer, MSocket> clientIDSocTable = null;
        private Hashtable<Integer, String> rev_clientIDTable = null;
        private Hashtable<String, MSocket> clientSocTable = null;

        /**
         * A queue of events.
         */
        private BlockingQueue toSendQueue = null;

        
        private CopyOnWriteArrayList<MPacket> replyQueue = null;
        
        private CopyOnWriteArrayList<MPacket> requestQueue = null;
        
        private BlockingQueue executeQueue = null;

        private CopyOnWriteArrayList<MPacket> notACKedQueue = null;

        private CopyOnWriteArrayList<MPacket> toExecuteQueue = null;

        private AtomicInteger timestamp = new AtomicInteger(0);
        
        private AtomicInteger missileTickOwner = new AtomicInteger(0);

        private AtomicInteger localSequenceNumber = new AtomicInteger(0);

        //Broadcast global sequence number
        private AtomicInteger BGSN = new AtomicInteger(0);

        //Expected global sequence number
        private AtomicInteger EGSN = new AtomicInteger(1);

        /**
         * The panel that displays the {@link Maze}.
         */
        private OverheadMazePanel overheadPanel = null;

        /**
         * The table the displays the scores.
         */
        private JTable scoreTable = null;
        
        /** 
         * Create the textpane statically so that we can 
         * write to it globally using
         * the static consolePrint methods  
         */
        private static final JTextPane console = new JTextPane();
      
        /** 
         * Write a message to the console followed by a newline.
         * @param msg The {@link String} to print.
         */ 
        public static synchronized void consolePrintLn(String msg) {
                console.setText(console.getText()+msg+"\n");
        }
        
        /** 
         * Write a message to the console.
         * @param msg The {@link String} to print.
         */ 
        public static synchronized void consolePrint(String msg) {
                console.setText(console.getText()+msg);
        }
        
        /** 
         * Clear the console. 
         */
        public static synchronized void clearConsole() {
           console.setText("");
        }
        
        /**
         * Static method for performing cleanup before exiting the game.
         */
        public static void quit() {
                // Put any network clean-up code you might have here.
                // (inform other implementations on the network that you have 
                //  left, etc.)
                

                System.exit(0);
        }
       
        /** 
         * The place where all the pieces are put together. 
         */
        public Mazewar(String host, int port, String namingServer, int serverPort) throws IOException,
                                                ClassNotFoundException {
                super("ECE419 Mazewar");
                consolePrintLn("ECE419 Mazewar started!");
                playerHostName = host;
                playerPort = port;
                // Create the maze
                maze = new MazeImpl(new Point(mazeWidth, mazeHeight), mazeSeed);
                assert(maze != null);
                // Have the ScoreTableModel listen to the maze to find
                // out how to adjust scores.
                ScoreTableModel scoreModel = new ScoreTableModel();
                assert(scoreModel != null);
                maze.addMazeListener(scoreModel);
                
                // Throw up a dialog to get the GUIClient name.
                name = JOptionPane.showInputDialog("Enter your name");
                if((name == null) || (name.length() == 0)) {
                  Mazewar.quit();
                }
                lSocket = new MServerSocket(port, host);
                nSocket = new Socket(InetAddress.getByName(namingServer), serverPort);
                //mSocket = new MSocket(host, port);

                //mSocket = new MSocket(serverHost, serverPort);
                //mSocketList = new MSocket[MAX_CLIENTS];
                //Send hello packet to server
                MPacket hello = new MPacket(name, MPacket.HELLO, MPacket.HELLO_INIT);
                hello.mazeWidth = mazeWidth;
                hello.mazeHeight = mazeHeight;
                hello.senderHostName = host;
                hello.senderPort = port;

                if(Debug.debug) System.out.println("Sending hello");
                out = new ObjectOutputStream(nSocket.getOutputStream());
                out.writeObject(hello);
                if(Debug.debug) System.out.println("hello sent");
                //Receive response from server
                System.out.println("Waiting for Server Hello Response");
                in = new ObjectInputStream(nSocket.getInputStream());
                System.out.println("Response from Server read");
                MPacket resp = (MPacket) in.readObject();
                if(Debug.debug) System.out.println("Received response from server");

                //Initialize queue of events
                toSendQueue = new LinkedBlockingQueue<MPacket>();
                //Modify this with the new comparator!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                replyQueue = new CopyOnWriteArrayList<MPacket>();
                requestQueue = new CopyOnWriteArrayList<MPacket>();
                toExecuteQueue = new CopyOnWriteArrayList<MPacket>();
                executeQueue = new LinkedBlockingQueue<MPacket>();
                notACKedQueue = new CopyOnWriteArrayList<MPacket>();
                
                //Initialize hash table of clients to client name 
                clientTable = new Hashtable<String, Client>();
                clientIDTable = new Hashtable<String, Integer>();
                rev_clientIDTable = new Hashtable<Integer, String>();
                clientSocTable = new Hashtable<String, MSocket>();
                clientIDSocTable = new Hashtable<Integer, MSocket>();
                
                // Create the GUIClient and connect it to the KeyListener queue
                //RemoteClient remoteClient = null;
                players = resp.players;
                for(Player player: resp.players){
                        clientIDTable.put(player.name, player.id);
                        rev_clientIDTable.put(player.id, player.name);
                        if(player.name.equals(name)){
                        	if(Debug.debug)System.out.println("Adding guiClient: " + player);
                                guiClient = new GUIClient(name, this.MAX_CLIENTS, requestQueue, toSendQueue, player.id, timestamp, localSequenceNumber);
                                maze.setMazeCreator(guiClient);
                                maze.setEventQueue(toSendQueue);
                                maze.addClientAt(guiClient, player.point, player.direction);
                                this.addKeyListener(guiClient);
                                this.id = player.id;
                                clientTable.put(player.name, guiClient);
                        }else{
                        	if(Debug.debug)System.out.println("Adding remoteClient: " + player);
                                RemoteClient remoteClient = new RemoteClient(player.name);
                                maze.addClientAt(remoteClient, player.point, player.direction);
                                clientTable.put(player.name, remoteClient);
                        }
                }
                
                // Use braces to force constructors not to be called at the beginning of the
                // constructor.
                /*
                {
                        maze.addClient(new RobotClient("Norby"));
                        maze.addClient(new RobotClient("Robbie"));
                        maze.addClient(new RobotClient("Clango"));
                        maze.addClient(new RobotClient("Marvin"));
                }
                */

                
                // Create the panel that will display the maze.
                overheadPanel = new OverheadMazePanel(maze, guiClient);
                assert(overheadPanel != null);
                maze.addMazeListener(overheadPanel);
                
                // Don't allow editing the console from the GUI
                console.setEditable(false);
                console.setFocusable(false);
                console.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder()));
               
                // Allow the console to scroll by putting it in a scrollpane
                JScrollPane consoleScrollPane = new JScrollPane(console);
                assert(consoleScrollPane != null);
                consoleScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Console"));
                
                // Create the score table
                scoreTable = new JTable(scoreModel);
                assert(scoreTable != null);
                scoreTable.setFocusable(false);
                scoreTable.setRowSelectionAllowed(false);

                // Allow the score table to scroll too.
                JScrollPane scoreScrollPane = new JScrollPane(scoreTable);
                assert(scoreScrollPane != null);
                scoreScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Scores"));
                
                // Create the layout manager
                GridBagLayout layout = new GridBagLayout();
                GridBagConstraints c = new GridBagConstraints();
                getContentPane().setLayout(layout);
                
                // Define the constraints on the components.
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1.0;
                c.weighty = 3.0;
                c.gridwidth = GridBagConstraints.REMAINDER;
                layout.setConstraints(overheadPanel, c);
                c.gridwidth = GridBagConstraints.RELATIVE;
                c.weightx = 2.0;
                c.weighty = 1.0;
                layout.setConstraints(consoleScrollPane, c);
                c.gridwidth = GridBagConstraints.REMAINDER;
                c.weightx = 1.0;
                layout.setConstraints(scoreScrollPane, c);
                                
                // Add the components
                getContentPane().add(overheadPanel);
                getContentPane().add(consoleScrollPane);
                getContentPane().add(scoreScrollPane);
                
                // Pack everything neatly.
                pack();

                // Let the magic begin.
                setVisible(true);
                overheadPanel.repaint();
                this.requestFocusInWindow();
        }

        /*
        *Starts the ClientSenderThread, which is 
         responsible for sending events
         and the ClientListenerThread which is responsible for 
         listening for events
        */
        private void startThreads(){
                //Start a new sender thread

                for(int i = 0; i < players.length; i++){
                        MSocket cSocket = null;
                        try {
                                if(this.name.equals(players[i].name)){
                                        for(int j = 0; j < players.length - 1; j++){
                                                //System.out.println("player length: "+players.length);
                                                cSocket = lSocket.accept();

                                                //new Thread(new ClientListenerThread(cSocket, clientTable, clientIDTable, toSendQueue, maze, players, name, requestQueue, replyQueue, timestamp, this.missileTickOwner, this.localSequenceNumber, this.notACKedQueue)).start();
                                                new Thread(new ClientListenerThread(cSocket, this.MAX_CLIENTS, clientTable, clientIDTable, toSendQueue, maze, players, this.name, requestQueue, replyQueue, toExecuteQueue, timestamp, this.missileTickOwner, this.localSequenceNumber, this.BGSN, this.EGSN, this.notACKedQueue, this.clientSocTable)).start();
                                        }
                                        //System.out.println("clientSocTable: "+clientSocTable);
                                }
                                else{
                                        cSocket = new MSocket(players[i].playerHostName, players[i].playerPort);
                                        clientSocTable.put(players[i].name, cSocket);
                                        clientIDSocTable.put(i, cSocket);
                                        mSocket.add(cSocket);
                                        MPacket connect = new MPacket(name, MPacket.HELLO, MPacket.HELLO_INIT);
                                        //System.out.println("Sending HELLO init");
                                        cSocket.writeObject(connect);
                                }

                        } catch (IOException e) {
                                e.printStackTrace();
                        }

                }

                //Start a new sender thread
                //new Thread(new ClientSenderThread(mSocket, eventQueue)).start();
                //Start a new sender thread
                //sSocket = new MScoket(playerHostName,playerPort);
                //new Thread(new ClientListenerThread(sSocket, clientTable, clientIDTable, eventQueue, maze, players, this.name, requestQueue, waitingQueue, replyQueue, eventQueue)).start();
                
                new Thread(new ClientSenderThread(mSocket, this.MAX_CLIENTS, toSendQueue, clientTable, clientIDTable, clientSocTable, timestamp, notACKedQueue)).start();
                
                //new Thread(new handleRequestQueueThread(mSocket, toSendQueue, requestQueue, replyQueue, clientTable, clientIDTable, clientSocTable,timestamp)).start();
                //Start a new listener thread
                //new Thread(new ClientListenerThread(mSocket, clientTable, clientIDTable, maze)).start();
                
                new Thread(new missileTickThread(mSocket, this.MAX_CLIENTS, clientTable, clientIDTable, rev_clientIDTable, toSendQueue, requestQueue, this.name, maze, this.missileTickOwner, this.localSequenceNumber)).start();
                
                new Thread(new NetworkErrorHandler(notACKedQueue, toSendQueue, this.id)).start();
                //new Thread( new missileTickThread(mSocket, clientTable, clientIDTable, rev_clientIDTable, toSendQueue, this.name, maze, this.missileTickOwner));
                //starting from the first client i.e. client with index 0
                if(clientIDTable.get(this.name)== 0){
                	this.missileTickOwner.set(0);
                }
        }

        
        /**
         * Entry point for the game.  
         * @param args Command-line arguments.
         */
        public static void main(String args[]) throws IOException,
                                        ClassNotFoundException{
             String host = args[0];
             int port = Integer.parseInt(args[1]);
             String namingServer = args[2];
             int serverPort = Integer.parseInt(args[3]);
             if(args.length < 4){
                     System.out.println("Usage ClientHostName ClientPort NamingServer NamingServerPort");
                     return;
             }
             /* Create the GUI */
             Mazewar mazewar = new Mazewar(host, port, namingServer, serverPort);
             mazewar.startThreads();
        }
}
