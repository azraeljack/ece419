/**
 * Created by peter on 07/03/16.
 */

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

public class NetworkErrorHandler implements Runnable{

    private CopyOnWriteArrayList<MPacket> notACKedQueue = null;
    private Hashtable<Integer, MSocket> clientIDSocTable = null;
    private BlockingQueue<MPacket> toSendQueue = null;
    private final int MAXCLIENTS = 2;
    private int selfID;

    public NetworkErrorHandler(CopyOnWriteArrayList<MPacket> notACKedQueue,
                               //Hashtable<Integer, MSocket> clientIDSocTable,
                               BlockingQueue<MPacket> toSendQueue,
                               int selfID){

        this.notACKedQueue = notACKedQueue;
        //this.clientIDSocTable = clientIDSocTable;
        this.toSendQueue = toSendQueue;
        this.selfID = selfID;
    }


    public void run() {
        while(true) {
            long currentTime = System.currentTimeMillis();
            ArrayList<ResendPacket> resend = new ArrayList<ResendPacket>();
            if(notACKedQueue.isEmpty())
                continue;
            System.out.println(notACKedQueue.toString());
            for(MPacket item : notACKedQueue){
                boolean toSend[] = new boolean[MAXCLIENTS];
                for(int i = 0; i < item.acked.length; i++){
                    toSend[i] = false;
                    if(i != selfID && item.acked[i] == 0 && currentTime - item.physicalTimestamp >= 1500){
                        toSend[i] = true;
                    }
                    //System.out.println("selfID is " + selfID + " Check send " + i + " " + toSend[i]);
                }
                for(int j = 0; j < toSend.length; j++){
                    if(toSend[j]) {
                        resend.add(new ResendPacket(item, toSend, notACKedQueue.indexOf(item)));
                        break;
                    }
                }
            }
            if(resend.isEmpty())
                continue;
            for(ResendPacket r : resend){
                r.packetToSend.physicalTimestamp = System.currentTimeMillis();
                r.packetToSend.resend = true;
                notACKedQueue.set(r.position, r.packetToSend);
                for(int i = 0; i < MAXCLIENTS; i++){
                    if(r.resend[i] && i!=selfID){
                        //clientIDSocTable.get(i).writeObject(r.packetToSend);
                        toSendQueue.add(r.packetToSend);
                        System.out.println("Resend packet " + r.packetToSend.toString());
                    }
                }
            }
            /*try{
                Thread.sleep(200);
            }catch(InterruptedException e){
                e.printStackTrace();
            }*/
        }
    }

    private class ResendPacket {
        public MPacket packetToSend = null;
        public boolean resend[] = null;
        public int position = -1;

        public ResendPacket(MPacket packetToSend, boolean resend[], int position){
            this.packetToSend = packetToSend;
            this.resend = resend;
            this.position = position;
        }
    }

}
