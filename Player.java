import java.io.Serializable;
import java.net.InetAddress;

public class Player implements Serializable {
    //Need these because direction is not serializable
    public final static int North = 0;
    public final static int South = 1;
    public final static int East  = 2;
    public final static int West  = 3;
    
    public Point point = null;
    public int direction;
    public String name;
    public int id;
    public String playerHostName = null;
    public int playerPort;
    
    public Player(String name, Point point, int direction, int id){
        this.point = point;
        this.name = name;
        this.direction = direction;
        this.id = id;
        this.playerHostName = "localhost";
        this.playerPort = 7000;
    }
    public Player(String name, Point point, int direction, int id, String playerHostName){
        this.point = point;
        this.name = name;
        this.direction = direction;
        this.id = id;
        this.playerHostName = playerHostName;
        this.playerPort = 7000;
    }
    public Player(String name, Point point, int direction, int id, String playerHostName, int playerPort){
        this.point = point;
        this.name = name;
        this.direction = direction;
        this.id = id;
        this.playerHostName = playerHostName;
        this.playerPort = playerPort;
    }
    public String toString(){
    	return "[" + name + " " + id + " : (" + point.getX() + "," + point.getY() + ")]";
    }

}