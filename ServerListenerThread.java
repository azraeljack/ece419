import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ServerListenerThread implements Runnable {

    private ObjectInputStream in = null;

    private Socket mSocket =  null;
    private BlockingQueue eventQueue = null;

    public ServerListenerThread(Socket mSocket, BlockingQueue eventQueue){
        this.mSocket = mSocket;
        this.eventQueue = eventQueue;
    }

    public void run() {
        MPacket received = null;
        if(Debug.debug) System.out.println("Starting a listener");
        while(true){
            try{
                in = new ObjectInputStream(mSocket.getInputStream());
                received = (MPacket) in.readObject();
                if(Debug.debug) System.out.println("Received: " + received);
                eventQueue.put(received);    
            }catch(InterruptedException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
            
        }
    }
}
