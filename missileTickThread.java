import java.io.InvalidObjectException;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.lang.Thread;
import java.net.Socket;

public class missileTickThread implements Runnable {

    //private ObjectOutputStream[] outputStreamList = null;
    private ArrayList<MSocket> mSocketList = null;
    private Hashtable<String, Client> clientTable;
    private Hashtable<String, Integer> clientIDTable;
	private Hashtable<Integer, String> rev_clientIDTable;
    private BlockingQueue toSendQueue = null;
    private int globalmissleSequenceNumber;
    private String name;
    private Maze maze = null;
    private AtomicInteger missileTickOwner;
    private int MAX_CLIENTS;
    private AtomicInteger localSendSequenceNumber;
    private CopyOnWriteArrayList<MPacket> requestQueue = null;


    public missileTickThread(ArrayList<MSocket> mSocketList,
    						int MAX_CLIENTS,
							Hashtable<String, Client> clientTable,
							Hashtable<String, Integer> clientIDTable,
							Hashtable<Integer, String> rev_clientIDTable,
							BlockingQueue toSendQueue,
					        CopyOnWriteArrayList<MPacket> requestQueue,
							String name,
							Maze maze,
							AtomicInteger missileTickOwner,
							AtomicInteger localSendSequenceNumber){
    	
        this.mSocketList = mSocketList;
        this.MAX_CLIENTS = MAX_CLIENTS;
        this.clientTable = clientTable;
        this.clientIDTable = clientIDTable;
        this.rev_clientIDTable = rev_clientIDTable;
        this.toSendQueue = toSendQueue;
        this.requestQueue = requestQueue;
        this.globalmissleSequenceNumber = 0;
        this.name = name;
        this.maze = maze;
        this.missileTickOwner = missileTickOwner;
        this.localSendSequenceNumber = localSendSequenceNumber;
        //System.out.println("after constructor");
    }
    
    public void run() {
    	//System.out.println("Started Missle Tick Thread !!!!!!!!!!!!!!!!");
    	MPacket send = null;
    	
        //handleHello();

//        try{
//        	thread.sleep(5000);
//        }catch(InterruptedException e){
//        	Thread.currentThread().interrupt();
//        }
    	
        while(true){
        	if(missileTickOwner.get() == 1){
	            for(int i = 0; i < 5; i++){
	        		try{
	        			
	        			if(Debug.debug) System.out.println("GUI sending action : UP");
                        MPacket packet = new MPacket(this.name, MPacket.ACTIONREQUEST, MPacket.MISSILETICK);
                        packet.localSendSequenceNumber = localSendSequenceNumber.get();
                        packet.requestLocalSequenceNumber = localSendSequenceNumber.get();
                        packet.replies=new int[MAX_CLIENTS];
						//initialize all responses to 0
						for(int j = 0; j<MAX_CLIENTS;j++){
						    packet.replies[j]=0;
						}
                        requestQueue.add(packet);
                        packet.communication= MPacket.MULTICAST;
                        toSendQueue.put(packet);
                        
                        this.localSendSequenceNumber.incrementAndGet();
	        			//System.out.println("Sending missile Tick");
//		                send = new MPacket(this.name, MPacket.ACTION, MPacket.MISSILETICK, this.name, MPacket.MULTICAST);
//		                toSendQueue.put(send);
//		                maze.missileTick();
		                //sleep for 200ms    
		                Thread.sleep(200);
		            }catch(InterruptedException e){
		                System.out.println("Throwing Interrupt");
		                e.printStackTrace();
		            }
	            }
	            int nextClientID = clientIDTable.get(this.name)+1;
	            
	            if(nextClientID == MAX_CLIENTS)
	            	nextClientID = 0;
	            //else if(nextClientID > MAX_CLIENTS)
	            	//System.out.println("Wrong missileTick pass");
	            
	            //System.out.println("handing to next client: "+nextClientID);
	            String sendto = rev_clientIDTable.get(nextClientID);
	            
	            try {
					toSendQueue.put(new MPacket(this.name, MPacket.MISSILETICKEOWNER, MPacket.MISSILETICK, sendto, MPacket.UNICAST));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	            this.missileTickOwner.set(0);
        	}

        }
    }
    /*
     *Handle the initial joining of players including
      position initialization
     */
//    public void handleHello(){
//
//        //The number of players
//        int playerCount = mSocketList.length;
//        Random randomGen = null;
//        Player[] players = new Player[playerCount];
//        if(Debug.debug) System.out.println("In handleHello");
//        MPacket hello = null;
//        try{
//            for(int i=0; i<playerCount; i++){
//                hello = (MPacket)eventQueue.take();
//                //Sanity check
//                if(hello.type != MPacket.HELLO){
//                    throw new InvalidObjectException("Expecting HELLO Packet");
//                }
//                if(randomGen == null){
//                   randomGen = new Random(hello.mazeSeed);
//                }
//                //Get a random location for player
//                Point point =
//                    new Point(randomGen.nextInt(hello.mazeWidth),
//                          randomGen.nextInt(hello.mazeHeight));
//
//                //Start them all facing North
//                Player player = new Player(hello.name, point, Player.North);
//                players[i] = player;
//            }
//
//            hello.event = MPacket.HELLO_RESP;
//            hello.players = players;
//            //Now broadcast the HELLO
//            if(Debug.debug) System.out.println("Sending " + hello);
//            for(MSocket mSocket: mSocketList){
//                mSocket.writeObject(hello);
//            }
//        }catch(InterruptedException e){
//            e.printStackTrace();
//            Thread.currentThread().interrupt();
//        }catch(IOException e){
//            e.printStackTrace();
//            Thread.currentThread().interrupt();
//        }
//    }
    
   

}
